#!/bin/bash

#SBATCH --qos=priority
#SBATCH --job-name=tcmex_run_script
#SBATCH --account=ebm
#SBATCH --chdir=/home/tovogt/code/tc_winds/mexico/log
#SBATCH --output=run_script-%j.out
#SBATCH --error=run_script-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16

module load intel/2019.0-nopython

export CONDA_HOME=/home/tovogt/.local/share/miniforge3
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

source $CONDA_HOME/etc/profile.d/conda.sh
conda activate kriPy

cd /home/tovogt/code/tc_winds/mexico
echo $@ > log/run_script-${SLURM_JOB_ID}.args
SCRIPT_NAME=$1
shift
srun python -u "scripts/$SCRIPT_NAME" $@
