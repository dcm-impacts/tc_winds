
import os
import pathlib
import sys

import dask
import geopandas as gpd
import numpy as np
import pandas as pd
import tc_cost.util.io as u_io
import xarray as xr

INPUT_DIR = pathlib.Path("input")
CACHE_DIR = pathlib.Path("cache")
OUTPUT_DIR = pathlib.Path("output")

SHP_DIR = INPUT_DIR / "shapes_mex_2020" / "conjunto_de_datos"
SHP_FILE_L = SHP_DIR / "00l.shp"
SHP_FILE_LPR = SHP_DIR / "00lpr.shp"

RAIN_PERIOD = (1980, 2020)
RAIN_DIR = INPUT_DIR / "ERA5-Land"
RAIN_FILE = lambda y, m: RAIN_DIR / f"total_precipitation_ERA5-Land_{y:04d}{m:02d}_daily.nc"
RAIN_MEAN_FILE = lambda m: RAIN_DIR / f"total_precipitation_ERA5-Land_mean_{m:02d}_daily.nc"

MEX_BOUNDS = (-118.5, 14.4, -86.6, 32.9)

# 300,690 localidades in total, 1000 per task
LOCS_PER_TASK = 1000


def read_loc_shapes(start=0):
    # read LOCS_PER_TASK localidades starting with the specified entry
    cache_file = CACHE_DIR / "mexico_localidades_2020.shp"

    if cache_file.exists():
        print(f"Loading cached data from {cache_file} ...")
        return gpd.read_file(cache_file, rows=slice(start, start + LOCS_PER_TASK))

    df = (
        gpd.read_file(SHP_FILE_L)
        .merge(
            gpd.read_file(SHP_FILE_LPR),
            on=["CVE_ENT", "CVE_MUN", "CVE_LOC"],
            how="outer",
            suffixes=("", "_P"),
        )
    )

    for geoc in ["geometry", "geometry_P"]:
        df[geoc] = df[geoc].to_crs("epsg:4326")

    cvegeo_len = df["CVEGEO"].str.len().drop_duplicates().dropna().astype(int).values[0]

    mask = df["geometry"].isna()
    df.loc[mask, "geometry"] = df.loc[mask, "geometry_P"]
    df.loc[mask, "CVEGEO"] = df.loc[mask, "CVEGEO_P"].str.slice(0, cvegeo_len)
    df = df[["CVEGEO", "geometry"]].rename(columns={"CVEGEO": "mapa"})

    # this will convert all points to polygon-type shapes
    df["geometry"] = df["geometry"].buffer(0.1)

    print(f"Writing to {cache_file} ...")
    df.to_file(cache_file)

    return df.iloc[start:start + LOCS_PER_TASK]


def load_rainfall_ds(bounds):
    assert bounds[2] < 0

    ds = xr.open_mfdataset([
        RAIN_FILE(y, m)
        for y in range(RAIN_PERIOD[0], RAIN_PERIOD[1] + 1)
        for m in range(1, 13)
    ])
    with dask.config.set(**{"array.slicing.split_large_chunks": False}):
        ds = ds.sel(longitude=ds["longitude"].values[ds["longitude"].values >= 180])
    ds["longitude"] = ds["longitude"] - 360

    lon_mask = (ds["longitude"].values >= bounds[0]) & (ds["longitude"].values <= bounds[2])
    lat_mask = (ds["latitude"].values >= bounds[1]) & (ds["latitude"].values <= bounds[3])
    with dask.config.set(**{"array.slicing.split_large_chunks": False}):
        ds = ds.sel(
            longitude=ds["longitude"].values[lon_mask],
            latitude=ds["latitude"].values[lat_mask],
        )

    return ds


def xr_unstack_time(da, time_dim_name, levels):
    da = (
        da
        .assign_coords({l: da[f"{time_dim_name}.{l}"] for l in levels})
        .set_index({time_dim_name: levels})
    )
    if len(levels) > 1:
        return da.unstack(time_dim_name)
    else:
        return da.rename(dict(time=levels[0]))


def compute_grid_stats():
    cache_file = CACHE_DIR / "precip_stats_mexico_grid.nc"

    if cache_file.exists():
        print(f"Loading cached data from {cache_file} ...")
        return xr.open_dataset(cache_file)

    ds = load_rainfall_ds(MEX_BOUNDS)

    # rainfall statistics per grid cell:
    # * total monthly rainfall: sum of daily rainfall over each month in each year
    da_rmon = ds["tp"].resample(dict(time="1m")).sum()
    da_rmon = xr_unstack_time(da_rmon, "time", ["year", "month"])

    # * total annual rainfall: sum of daily rainfall over whole year
    da_rann = da_rmon.sum(dim="month")

    # * number of wet days (> 1mm) per year
    da_wetdays = (ds["tp"] > 0.001).resample(dict(time="1y")).sum()

    # * 99.9th percentile of daily rainfall for whole period
    da_999perc = ds["tp"].chunk(dict(time=-1)).quantile(dim="time", q=0.999).drop_vars("quantile")

    # * extreme daily rainfall (annual total above 99.9th percentile) per year
    da_extreme = (ds["tp"] * (ds["tp"] >= da_999perc)).resample(dict(time="1y")).sum()

    ds = xr.Dataset({
        "rmon": da_rmon,
        "rann": da_rann,
        "wetdays": xr_unstack_time(da_wetdays, "time", ["year"]),
        "extreme": xr_unstack_time(da_extreme, "time", ["year"]),
        "area_weight": np.cos(np.radians(ds["latitude"]))
    })

    print(f"Writing to {cache_file} ...")
    encoding = {v: {'zlib': True} for v in ds.data_vars}
    ds.to_netcdf(cache_file, encoding=encoding)

    return ds


def compute_grid_points(ds):
    cache_file = CACHE_DIR / "mexico_points_era5.shp"

    if cache_file.exists():
        print(f"Loading cached data from {cache_file} ...")
        return gpd.read_file(cache_file).set_index(["longitude", "latitude"])

    gdf = ds[["longitude", "latitude"]].to_dataframe().reset_index()
    gdf = gpd.GeoDataFrame(
        data=gdf,
        geometry=gpd.points_from_xy(gdf["longitude"], gdf["latitude"]),
    ).set_index(["longitude", "latitude"])

    print(f"Writing to {cache_file} ...")
    gdf.to_file(cache_file)

    return gdf


def compute_regional_stats(ds):
    # regional statistics, area-weighted mean of grid cell statistics:
    ds = ds.where(ds["region_mask"])
    ds["area_weight"] /= ds["area_weight"].sum()

    for v in ["rann", "rmon", "wetdays", "extreme"]:
        ds[v] = (ds["area_weight"] * ds[v]).mean(dim=["longitude", "latitude"])

    # * historical mean of annual and monthly rainfall totals
    ds["rmon_mean"] = ds["rmon"].mean(dim=["year"])
    ds["rmon_std"] = ds["rmon"].std(dim=["year"])
    ds["rann_mean"] = ds["rann"].mean(dim=["year"])

    ds["rmon_dev"] = (
        ((ds["rmon"] - ds["rmon_mean"]) / ds["rmon_std"])
        * (ds["rmon_mean"] / ds["rann"])
    ).sum(dim=["month"])

    return ds[["rann", "wetdays", "extreme", "rmon_dev"]]


def main():
    try:
        task = int(os.environ["SLURM_ARRAY_TASK_ID"])
    except:
        task = int(sys.argv[-1])

    start = task * LOCS_PER_TASK
    end = start + LOCS_PER_TASK - 1

    path = OUTPUT_DIR / "precip_stats_annual_by_loc_nc" / f"{start:06d}-{end:06d}.nc"

    if u_io.path_exists_safely(path):
        return

    ds = compute_grid_stats()
    gpd_points = compute_grid_points(ds)
    df_shp = read_loc_shapes(start=start)

    ds_reg_all = []
    for idx, row in df_shp.iterrows():
        ds["region_mask"] = gpd_points.within(row["geometry"]).to_xarray()
        ds_reg = compute_regional_stats(ds)
        ds_reg["mapa"] = (["mapa"], [row["mapa"]])
        ds_reg_all.append(ds_reg)
    ds = xr.concat(ds_reg_all, dim="mapa")

    u_io.write_safely(ds, path)


if __name__ == "__main__":
    main()
