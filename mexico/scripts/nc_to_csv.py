
import pathlib

import numpy as np
import tc_cost.util.io as u_io
import xarray as xr

OUTPUT_DIR = pathlib.Path("output")
NC_DIR = OUTPUT_DIR / "precip_stats_annual_by_loc_nc"
NC_FILE = OUTPUT_DIR / "precip_stats_annual_by_loc.nc"
CSV_DIR = OUTPUT_DIR / "precip_stats_annual_by_loc_csv"


def main():
    if not u_io.path_exists_safely(NC_FILE):
        paths = sorted([str(p) for p in NC_DIR.glob("*.nc")])
        # open_mfdataset seems to leak memory when there are too many files at a time
        # hence we manually load the data in chunks, and then merge
        chunksize = 100
        nchunks = int(np.ceil(len(paths) / chunksize))
        ds_arr = [
            xr.open_mfdataset(
                paths[i * chunksize:(i + 1) * chunksize], combine="nested",
            ).compute()
            for i in range(nchunks)
        ]
        u_io.write_safely(xr.merge(ds_arr), NC_FILE)
    ds = xr.open_dataset(NC_FILE)

    for v in ds.data_vars:
        path = CSV_DIR / f"{v}.csv"
        print(f"Writing to {path} ...")
        ds[v].to_pandas().to_csv(path)


if __name__ == "__main__":
    main()
