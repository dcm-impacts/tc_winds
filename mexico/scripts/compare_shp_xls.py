import pathlib

import geopandas as gpd
import numpy as np
import pandas as pd


DIR_SHP = pathlib.Path("shapes_mex_2020") / "conjunto_de_datos"
PATH_SHP_L = DIR_SHP / "00l.shp"
PATH_SHP_LPR = DIR_SHP / "00lpr.shp"

PATH_XLS = "Codigos_municipios_Mexico.xlsx"


def check_merged_shp(df):
    # urban areas are only in "00l.shp":
    assert df[(df["AMBITO"] == "Urbana") & ~df["CVEGEO_P"].isna()].size == 0

    # all rural areas are also contained in "00lpr.shp"
    assert df[(df["AMBITO"] == "Rural") & df["CVEGEO_P"].isna()].size == 0

    mask_intersect = (~df["CVEGEO"].isna() & ~df["CVEGEO_P"].isna())

    # all points are contained in the respective polygons (if there are any), up to 0.1 degree precision
    assert df[
        mask_intersect
        & ~df["geometry_P"].within(df["geometry"].buffer(0.05))
    ].size == 0

    # matching localidades have the same names assigned to them
    assert df[mask_intersect & (df["NOMGEO"] != df["NOMGEO_P"])].size == 0

    # matching localidades have the same IDs assigned to them
    cvegeo_len = df["CVEGEO"].str.len().drop_duplicates().dropna().astype(int).values[0]
    assert df[mask_intersect & (df["CVEGEO"] != df["CVEGEO_P"].str.slice(0, cvegeo_len))].size == 0

    mask = df["CVEGEO"].isna()
    df.loc[mask, "NOMGEO"] = df.loc[mask, "NOMGEO_P"]
    df.loc[mask, "CVEGEO"] = df.loc[mask, "CVEGEO_P"].str.slice(0, cvegeo_len)

    # all CVEGEO identifiers are unique
    assert df["CVEGEO"].duplicated().sum() == 0


def load_shp():
    df = (
        gpd.read_file(PATH_SHP_L)
        .merge(
            gpd.read_file(PATH_SHP_LPR),
            on=["CVE_ENT", "CVE_MUN", "CVE_LOC"],
            how="outer",
            suffixes=("", "_P"),
        )
    )
    for geoc in ["geometry", "geometry_P"]:
        df[geoc] = df[geoc].to_crs("epsg:4326")

    check_merged_shp(df)

    df = df.drop(columns=[
        "CVEGEO_P", "NOMGEO_P", "CVE_AGEB", "CVE_MZA", "PLANO",
    ])
    df["AMBITO"] = df["AMBITO"].fillna("Rural")

    return df


def convert_shp_format(df):
    # convert format (column names etc.) of SHP data to that of the XLS data
    id_columns = ["CVEGEO", "CVE_ENT", "CVE_MUN", "CVE_LOC"]
    for c in id_columns:
        df[c] = df[c].astype(int)
    id_columns_rename = {c: c.lower() for c in id_columns}
    id_columns_rename["CVEGEO"] = "mapa"
    id_columns_rename["NOMGEO"] = "nom_loc"
    id_columns_rename["AMBITO"] = "ámbito"
    df = df.rename(columns=id_columns_rename)
    df["ámbito"] = df["ámbito"].str.slice(0, 1)
    df = df.sort_values(by="mapa").reset_index(drop=True)
    df["nom_loc"] = df["nom_loc"].str.replace("\xa0", " ").str.strip()
    return df


def load_xls():
    df = pd.read_excel(PATH_XLS, skiprows=3)
    return gpd.GeoDataFrame(
        data=df,
        geometry=gpd.points_from_xy(
            df["lon_decimal"],
            df["lat_decimal"],
            crs="epsg:4326"
        ),
    )


def main():
    df_shp = convert_shp_format(load_shp())
    df_xls = load_xls()

    # check that the localidades contained in the two datasets are identical
    assert (df_xls["mapa"] == df_shp["mapa"]).all()
    assert (df_xls["nom_loc"] == df_shp["nom_loc"]).all()
    assert (df_xls["ámbito"] == df_shp["ámbito"]).all()

    df = df_shp.merge(df_xls[["mapa", "geometry"]], on="mapa", suffixes=("", "_X"), how="left")
    assert df["geometry_P"].distance(df["geometry_X"]).max() < 1e-2


if __name__ == "__main__":
    main()
