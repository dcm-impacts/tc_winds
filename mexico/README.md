
input/Codigos_municipios_Mexico.xlsx
- obtained from José on 2022-11-09
- list of localidades, apparently from January 2021 version of some INEGI data

input/889463807469_s.zip
- downloaded on 2022-11-16 from https://www.inegi.org.mx/app/biblioteca/ficha.html?upc=889463807469
- version (2020) of the Marco Geoestadístico that most probably matches the data obtained from José

scripts/compare_shp_xls.py
- Check that the localidades by José agree with the ones in the INEGI shape files
