
import argparse
import itertools
import logging
import os
import pathlib
import pickle
import warnings

from climada.hazard import Centroids, TCTracks, TropCyclone
from climada.hazard.trop_cyclone import KN_TO_MS
import climada.util.coordinates as u_coord
import numpy as np
import pandas as pd
import rasterio
import shapely.geometry
import xarray as xr


warnings.filterwarnings(
    "ignore",
    message="Index.ravel returning ndarray is deprecated; in a future version",
    module="xarray",
    category=FutureWarning,
)
warnings.filterwarnings(
    "ignore",
    message="invalid value encountered in cast",
    module="xarray",
    category=RuntimeWarning,
)


EPSG_4326 = "epsg:4326"

OUTPUT_DIR = pathlib.Path("output")
CACHE_DIR = pathlib.Path("cache")

# minimum winds to consider (in knots)
MINWIND_KN = 0.1

RAINFALL_INPUT_DIR = pathlib.Path("/p/projects/ebm/data/rainfall/")

# original hourly data: /p/projects/climate_data_central/reanalysis/ERA5-Land/total_precipitation/
# aggregate daily data:
ERA5L_PATH = RAINFALL_INPUT_DIR /  "ERA5-Land"
ERA5L_FNAME = "total_precipitation_ERA5-Land_{year:04d}{month:02d}_daily.nc"

# original hourly data: /p/projects/climate_data_central/reanalysis/ERA5/pr/
# aggregate daily data:
ERA5_PATH = RAINFALL_INPUT_DIR /  "ERA5-daily"
ERA5_FNAME = "pr_daily_ECMWF-ERA5_observation_1979010107-2021123123.nc"


def dt64_to_dmy(date):
    year = date.astype('datetime64[Y]').astype(int) + 1970
    month = date.astype('datetime64[M]').astype(int) % 12 + 1
    day = (date - date.astype('datetime64[M]') + 1).astype(int)
    return day, month, year


def era5l_daily_rainf(date, bounds, spatial_max=True):
    # in ERA5-Land, rainfall at 00:00:00 is accumulated value from previous day
    date += np.timedelta64(1, "D")

    # 0.1 degree resolution, make sure we don't lose values close to boundary
    pad = 0.05
    bounds = (bounds[0] - pad, bounds[1] - pad, bounds[2] + pad, bounds[3] + pad)

    day, month, year = dt64_to_dmy(date)
    path = ERA5L_PATH / ERA5L_FNAME.format(year=year, month=month)
    if bounds[0] < 0:
        bounds = (bounds[0] + 360, bounds[1], bounds[2] + 360, bounds[3])
    with xr.open_dataset(path) as rainf_ds:
        rainf_ds = rainf_ds.rename({"longitude": "lon", "latitude": "lat"})
        rainf_ds = rainf_ds.sel(time=(rainf_ds.time.dt.day == day))
        if rainf_ds.time.size == 0:
            raise IndexError(f"Date not available in ERA5-Land: {year}-{month}-{day}")
        mask_bounds = ((bounds[1] <= rainf_ds.lat)
                       & (rainf_ds.lat <= bounds[3])
                       & (bounds[0] <= rainf_ds.lon)
                       & (rainf_ds.lon <= bounds[2]))
        rainf_ds = rainf_ds.where(mask_bounds, drop=True)
        lat, lon = rainf_ds.lat.values, rainf_ds.lon.values
        lon[lon > 180] -= 360
        dlon = lon[1] - lon[0]
        dlat = lat[1] - lat[0]
        transform = rasterio.Affine(dlon, 0, lon[0] - 0.5 * dlon,
                                    0, dlat, lat[0] - 0.5 * dlat)
        # convert from m to mm
        rainf = rainf_ds.tp.values[0, :, :] * 1000
    if not spatial_max:
        return rainf, transform
    return np.nanmax(rainf)


def era5_daily_rainf(date, bounds, spatial_max=True):
    # 0.25 degree resolution, make sure we don't lose values close to boundary
    pad = 0.125
    bounds = (bounds[0] - pad, bounds[1] - pad, bounds[2] + pad, bounds[3] + pad)

    path = ERA5_PATH / ERA5_FNAME
    if bounds[0] < 0:
        bounds = (bounds[0] + 360, bounds[1], bounds[2] + 360, bounds[3])
    with xr.open_dataset(path) as rainf_ds:
        rainf_ds = rainf_ds.rename({"longitude": "lon", "latitude": "lat"})
        rainf_ds = rainf_ds.sel(time=(rainf_ds.time.astype(date.dtype) == date))
        mask_bounds = ((bounds[1] <= rainf_ds.lat)
                       & (rainf_ds.lat <= bounds[3])
                       & (bounds[0] <= rainf_ds.lon)
                       & (rainf_ds.lon <= bounds[2]))
        rainf_ds = rainf_ds.where(mask_bounds, drop=True)
        lat, lon = rainf_ds.lat.values, rainf_ds.lon.values
        lon[lon > 180] -= 360
        dlon = lon[1] - lon[0]
        dlat = lat[1] - lat[0]
        transform = rasterio.Affine(dlon, 0, lon[0] - 0.5 * dlon,
                                    0, dlat, lat[0] - 0.5 * dlat)
        # convert from m to mm
        rainf = rainf_ds.pr.values[0, :, :] * 1000
    if not spatial_max:
        return rainf, transform
    return np.nanmax(rainf)


def era5_combined_daily_rainf_bounds(date, bounds, spatial_max=True):
    rainf, transform = era5_daily_rainf(date, bounds, spatial_max=False)
    rainf_l, transform_l = era5l_daily_rainf(date, bounds, spatial_max=False)

    rainf_fine = np.zeros_like(rainf_l)
    rasterio.warp.reproject(
        source=rainf,
        destination=rainf_fine,
        src_transform=transform,
        src_crs=EPSG_4326,
        dst_transform=transform_l,
        dst_crs=EPSG_4326,
        resampling=rasterio.warp.Resampling.nearest)

    nan_mask = np.isnan(rainf_l)
    rainf_l[nan_mask] = rainf_fine[nan_mask]
    if not spatial_max:
        return rainf_l, transform_l
    return np.nanmax(rainf_l)


def era5_combined_daily_rainf(date, lat, lon):
    pad = 0.2
    bounds = (lon.min() - pad, lat.min() - pad, lon.max() + pad, lat.max() + pad)
    rainf, transform = era5_combined_daily_rainf_bounds(date, bounds, spatial_max=False)
    return u_coord.interp_raster_data(rainf, lat, lon, transform, method="nearest")


def generate_centroids(bounds, res_deg):
    # generate centroids grid within rectangular bounds
    lat_dim = np.arange(bounds[1] + 0.5 * res_deg, bounds[3], res_deg)
    lon_dim = np.arange(bounds[0] + 0.5 * res_deg, bounds[2], res_deg)

    centroids = Centroids(meta={
        "width": lon_dim.size,
        "height": lat_dim.size,
        "transform": rasterio.Affine(res_deg, 0, bounds[0],
                                     0, res_deg, bounds[1]),
        "crs": EPSG_4326,
    })
    centroids.set_meta_to_lat_lon()
    centroids.set_dist_coast(precomputed=True, signed=True)
    centroids.on_land = (centroids.dist_coast < 0)

    # define onland centroids
    centroids.region_id = centroids.on_land.copy().astype(int)
    centroids_land = centroids.select(reg_id=1)
    centroids_land.dist_coast[:] = np.abs(centroids_land.dist_coast)

    return centroids, centroids_land


def get_centroids(bounds, res_deg, fname):
    path = CACHE_DIR / f"{fname}.pickle"
    if path.exists():
        with open(path, "rb") as fp:
            centroids, centroids_land = pickle.load(fp)
    else:
        centroids, centroids_land = generate_centroids(bounds, res_deg)
        with open(path, "wb") as fp:
            pickle.dump((centroids, centroids_land), fp)
    return centroids, centroids_land


def get_tc_tracks_by_period(period):
    # Generate or load the data set of TC tracks
    # Load all tracks in IBTrACS within the given time period.
    path = CACHE_DIR / f"{period[0]}-{period[1]}_tracks.pickle"
    if path.exists():
        with open(path, "rb") as fp:
            tracks = pickle.load(fp)
    else:
        tracks = TCTracks.from_ibtracs_netcdf(
            year_range=period, estimate_missing=True, interpolate_missing=True,
            additional_variables=["nature"],
        )
        with open(path, "wb") as fp:
            pickle.dump(tracks, fp)
    print("Number of tracks in time period:", tracks.size)
    return tracks


def get_tc_tracks_by_region(tracks, bounding_box_land, fname):
    # Restrict to those tracks that cross the region of interest.
    path = CACHE_DIR / f"{fname}.pickle"
    if path.exists():
        with open(path, "rb") as fp:
            tracks = pickle.load(fp)
    else:
        for tr in tracks.data:
            lons = tr.lon.values.copy()
            oob_neg = np.any(lons <= -180)
            oob_pos = np.any(lons > 180)
            if oob_neg or oob_pos:
                lons_n = u_coord.lon_normalize(lons.copy(), center=180)
                if oob_pos and not np.any(lons_n != lons):
                    continue
                print(f"{'Negative' if oob_neg else 'Positive'} out-of-bounds: {tr.sid}")
                tr.lon.values[:] = lons_n

        # get tracks as shapely geometries to intersect with region of interest
        gdf = tracks.to_geodataframe(split_lines_antimeridian=True)

        # filter out those tracks that are too far off
        tr_sel = gdf.intersects(bounding_box_land).values
        tracks.data = [t for i, t in enumerate(tracks.data) if tr_sel[i]]

        # interpolate to 1-hour timesteps
        tracks.equal_timestep(time_step_h=1)

        with open(path, "wb") as fp:
            pickle.dump(tracks, fp)
    print("Number of tracks that cross the region of interest:", tracks.size)
    return tracks


def generate_windfield(track, centroids_land):
    tracks = TCTracks(data=[track])
    haz = TropCyclone.from_tracks(
        tracks, centroids=centroids_land, ignore_distance_to_coast=True,
        store_windfields=True, intensity_thres=MINWIND_KN * KN_TO_MS)

    # restrict to TCs with non-zero winds in region of interest
    if haz.intensity.sum(axis=1).A1[0]:
        return haz.windfields[0]
    return None


def write_daily_statistics(track, centroids_land, res_decimals, out_path, minwind_kn, kn_to_ms):
    out_path = out_path / f"{track.sid}.csv"
    if out_path.exists():
        return

    windfield = generate_windfield(track, centroids_land)
    if windfield is None:
        return

    days = np.array(track.time, dtype="datetime64[D]")

    # select only those track positions for which some centroids are affected
    affected_positions_mask = np.diff(windfield.indptr) > 0

    event_df = []
    for day in np.unique(days):
        # select track positions on selected day for which some centroids are affected
        day_mask = affected_positions_mask & (days == day)
        if not np.any(day_mask):
            continue

        # compute absolute intensity from wind vector field
        day_intensity = np.linalg.norm(
            windfield[day_mask, :].toarray().reshape(-1, centroids_land.size, 2),
            axis=-1)
        day_intensity_max = day_intensity.max(axis=0)

        # select only those centroids that are affected by minwind_kn winds
        mask = (day_intensity_max >= minwind_kn * kn_to_ms)
        if not np.any(mask):
            continue

        lat = centroids_land.lat[mask]
        lon = centroids_land.lon[mask]

        day_rainf = era5_combined_daily_rainf(day, lat, lon)

        day_intensity_mean = day_intensity.mean(axis=0)
        event_df.append(pd.DataFrame({
            "lat": np.round(lat, decimals=res_decimals),
            "lon": np.round(lon, decimals=res_decimals),
            "date": day,
            "windspeed_kn_mean": np.round(day_intensity_mean[mask] / kn_to_ms, decimals=1),
            "windspeed_kn_max": np.round(day_intensity_max[mask] / kn_to_ms, decimals=1),
            "rainfall_mm": np.round(day_rainf, decimals=0),
        }))

    # don't create record for events that didn't affect any land area
    if len(event_df) == 0:
        return
    pd.concat(event_df).to_csv(out_path, index=False)


def main(period, pnt_bounds, res_deg, exclude_et):
    # define a process pool (optional, for execution on a SLURM cluster)
    pool = None
    if 'OMP_NUM_THREADS' in os.environ and int(os.environ['OMP_NUM_THREADS']) > 1:
        from pathos.pools import ProcessPool as Pool
        print(f"Running with {os.environ['OMP_NUM_THREADS']} threads.")
        pool = Pool(nodes=int(os.environ['OMP_NUM_THREADS']))

    # build output base filename for hazard and exposure
    lon_min, lat_min, lon_max, lat_max = pnt_bounds
    pnt_bounds_str = '{:.4g}{}-{:.4g}{}_{:.4g}{}-{:.4g}{}'.format(
        abs(lat_min), 'N' if lat_min >= 0 else 'S',
        abs(lat_max), 'N' if lat_max >= 0 else 'S',
        abs(lon_min), 'E' if lon_min >= 0 else 'W',
        abs(lon_max), 'E' if lon_max >= 0 else 'W')
    tracks_base_fname = f"{period[0]}-{period[1]}_{pnt_bounds_str}"
    cen_base_fname = f"centroids_{pnt_bounds_str}_{int(res_deg * 3600)}as"
    haz_base_fname = f"{tracks_base_fname}_{int(res_deg * 3600)}as"
    if exclude_et:
        haz_base_fname = f"{haz_base_fname}_NOET"

    # determine number of significant digits for lat/lon coordinates
    res_decimals = 4
    for i in range(4):
        if (res_deg * 10**i) % 1 == 0.0:
            res_decimals = i
            break

    # bounds : bounds of the raster (instead of the points at the cell-centers)
    bounds = (pnt_bounds[0] - 0.5 * res_deg,
              pnt_bounds[1] - 0.5 * res_deg,
              pnt_bounds[2] + 0.5 * res_deg,
              pnt_bounds[3] + 0.5 * res_deg)

    centroids, centroids_land = get_centroids(bounds, res_deg, cen_base_fname)

    tracks = get_tc_tracks_by_period(period)

    bounding_box_land = shapely.geometry.box(*centroids_land.total_bounds).buffer(5.5).envelope
    tracks = get_tc_tracks_by_region(tracks, bounding_box_land, f"{tracks_base_fname}_tracks")

    if exclude_et:
        tracks = TCTracks(data=[
            tr.sel(time=(tr["nature"] != "ET"))
            for tr in tracks.data
            if (tr["nature"] != "ET").sum() > 3
        ])

    out_path = OUTPUT_DIR / haz_base_fname
    out_path.mkdir(exist_ok=True)

    if pool is None:
        for i_tr, track in enumerate(tracks.data):
            print(f"Track {i_tr + 1}/{tracks.size}: {track.sid}")
            write_daily_statistics(
                track, centroids_land, res_decimals, out_path, MINWIND_KN, KN_TO_MS)
    else:
        pool.map(write_daily_statistics, tracks.data,
                 itertools.repeat(centroids_land, tracks.size),
                 itertools.repeat(res_decimals, tracks.size),
                 itertools.repeat(out_path, tracks.size),
                 itertools.repeat(MINWIND_KN, tracks.size),
                 itertools.repeat(KN_TO_MS, tracks.size))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=(
        'Store daily TC-related windspeed and rainfall on grid.'
    ))
    parser.add_argument('--period', type=int, metavar="YEAR", nargs=2, default=(1980, 2019),
                        help='Time period (minyear, maxyear).')
    parser.add_argument('--resolution', metavar="DEG", type=float, default=0.1,
                        help=("Grid resolution in degrees."))
    parser.add_argument('--bounds', type=float, metavar="COORD", nargs=4,
                        default=(-180., -80., 180., 80.),
                        help=(
                            'Spatial bounds to restrict to (lon_min, lat_min, lon_max, lat_max). '
                            'Note that these are the bounds of the raster cell centers, '
                            'not the outer raster bounds.'
                        ))
    parser.add_argument('--exclude_et', action="store_true",
                        help='Exclude track positions after extratropical transition.')
    args = parser.parse_args()

    main(args.period, args.bounds, args.resolution, args.exclude_et)


