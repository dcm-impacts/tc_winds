#!/bin/bash

#SBATCH --qos=priority
#SBATCH --job-name=tc_daily_winds
#SBATCH --account=ebm
#SBATCH --chdir=/home/tovogt/code/tc_winds/daily_winds/log
#SBATCH --output=tc_daily_winds-%j.out
#SBATCH --error=tc_daily_winds-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16

module load intel/2019.0-nopython

export CONDA_HOME=/home/tovogt/.local/share/miniforge3
export OMP_NUM_THREADS=16

source $CONDA_HOME/etc/profile.d/conda.sh
conda activate isimip3a

cd /home/tovogt/code/tc_winds/daily_winds/
echo $@ > log/tc_daily_winds-${SLURM_JOB_ID}.args
python windfields.py $@
