
import os
import sys
import logging
from datetime import datetime
import numpy as np
import pandas as pd
import netCDF4 as nc

from climada.hazard import TCTracks, TropCyclone
from climada.entity import IFTropCyclone, ImpactFuncSet
from climada.engine import Impact
from climada.hazard.flood import RiverFlood
from climada.entity.exposures.litpop import LitPop

LOGGER = logging.getLogger('philippines')
LOGGER.setLevel(logging.DEBUG)
LOGGER.propagate = False
if LOGGER.hasHandlers():
    for handler in LOGGER.handlers:
        LOGGER.removeHandler(handler)
FORMATTER = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
CONSOLE = logging.StreamHandler(stream=sys.stdout)
CONSOLE.setFormatter(FORMATTER)
LOGGER.addHandler(CONSOLE)

BASIN = "WP"  # Western Pacific basin
COUNTRY_ISO = "PHL"  # Philippines
YEAR = 2015

def prepare_nc(lat, lon, path, var):
    """ Prepare a base NetCDF file for storing subbasin-specific TC data

    Parameters
    ----------
    lat, lon : ndarrays of floats
        Centroid coordinates.
    path : str
        Path to output file.

    Returns
    -------
    ncfile : nc.Dataset (opened as writable)
        Dimensions for event, longitude and latitude.
        More variables have to be added.
    nc_indls : ndarray of ints
        The centroids of the original data are not necessarily on a grid, but
        the NetCDF format forces the output to be on a (not necessarily regular)
        grid.
        This output is for identification between the original centroids and
        the grid cells in the NetCDF data. If the coordinates of the original
        centroids are stored in two arrays (lat, lon) and the NetCDF dimensions
        are (nc_lat, nc_lon), then:

        >>> yf, xf = [c.flatten() for c in np.meshgrid(nc_lat, nc_lon, indexing='ij')]
        >>> assert np.all(lat == yf[nc_indls])
        >>> assert np.all(lon == xf[nc_indls])
    """
    lat_uniq, lat_inv = np.unique(lat, return_inverse=True)
    lon_uniq, lon_inv = np.unique(lon, return_inverse=True)
    grid_shape = (lat_uniq.shape[0], lon_uniq.shape[0])
    nc_indls = np.ravel_multi_index((lat_inv, lon_inv), dims=grid_shape)

    try:
        os.remove(path)
    except FileNotFoundError:
        pass

    LOGGER.info(f"Preparing {path}...")
    ncfile = nc.Dataset(path, 'w', format='NETCDF4')
    ncfile.createDimension('lat', size=grid_shape[0])
    ncfile.createDimension('lon', size=grid_shape[1])
    ncfile.createDimension('time', size=None)

    lat_nc = ncfile.createVariable('lat', 'float64', ('lat',), zlib=True)
    lat_nc.units = 'degrees_north'
    lat_nc.standard_name = 'latitude'

    lon_nc = ncfile.createVariable('lon', 'float64', ('lon',), zlib=True)
    lon_nc.units = 'degrees_east'
    lon_nc.standard_name = 'longitude'

    time = ncfile.createVariable('time', 'float64', ('time',), zlib=True)
    time.units = 'days since 2015-01-01'
    time.calendar = 'gregorian'

    time = ncfile.createVariable('id', str, ('time',), zlib=True)
    time.standard_name = 'storm ID in ibtracs database'

    wind = ncfile.createVariable(
        var[0], 'float64', ('time', 'lat', 'lon'), zlib=True)
    wind.standard_name = var[1]
    wind.units = var[2]

    lat_nc[:] = lat_uniq
    lon_nc[:] = lon_uniq

    return ncfile, nc_indls


def tc_wind_haz():
    tr = TCTracks()
    tr.read_ibtracs_netcdf(year_range=(YEAR, YEAR), basin=BASIN)
    tr.equal_timestep(time_step_h=1)

    centroids = RiverFlood.select_exact_area(countries=[COUNTRY_ISO])
    tc = TropCyclone()
    tc.set_from_tracks(tr, centroids=centroids)
    tc.check()

    return tc


def write_windfield_nc(haz):
    path = f"windfields_{YEAR}_{COUNTRY_ISO}.nc"
    ncfile, nc_indls = prepare_nc(haz.centroids.lat, haz.centroids.lon, path,
                                  ('wind', 'TC wind speed', 'wind speed in m/s'))
    grid_shape = (ncfile['lat'].size, ncfile['lon'].size)

    LOGGER.info(f"Writing {len(haz.date)} windfields...")
    nc_i = 0
    for tc_i, tc_date in enumerate(haz.date):
        inten = haz.intensity[tc_i,:].toarray()
        if np.count_nonzero(inten > 0) == 0:
            continue

        tc_date = datetime.fromordinal(tc_date)
        nc_date = nc.date2num(tc_date, units=ncfile['time'].units,
                                       calendar=ncfile['time'].calendar)
        wind_nc = np.zeros(grid_shape).ravel()
        wind_nc[nc_indls] = inten

        ncfile['time'][nc_i] = nc_date
        ncfile['id'][nc_i] = haz.event_name[tc_i]
        ncfile['wind'][nc_i, :, :] = wind_nc.reshape(grid_shape)
        nc_i += 1
    ncfile.close()


def write_litpop_damage(haz):
    exp = LitPop()
    exp.set_country(COUNTRY_ISO, fin_mode='pc', res_arcsec=300, reference_year=YEAR)
    exp.gdf['impf_TC'] = np.ones(exp['value'].size)
    impf_tc = IFTropCyclone()
    impf_tc.set_emanuel_usa()
    impf_set = ImpactFuncSet()
    impf_set.append(impf_tc)
    impf_set.check()
    imp = Impact()
    imp.calc(exp, impf_set, haz, save_mat=True)

    path = f"damage_{YEAR}_{COUNTRY_ISO}.nc"
    ncfile, nc_indls = prepare_nc(exp.latitude, exp.longitude, path,
                                  ('damage_pc', 'damage to produced capital', '2014 USD'))
    grid_shape = (ncfile['lat'].size, ncfile['lon'].size)

    LOGGER.info(f"Writing {len(haz.date)} damage maps...")
    nc_i = 0
    for tc_i, tc_date in enumerate(haz.date):
        inten = haz.intensity[tc_i,:].toarray()
        if np.count_nonzero(inten > 0) == 0:
            continue

        tc_date = datetime.fromordinal(tc_date)
        nc_date = nc.date2num(tc_date, units=ncfile['time'].units,
                                       calendar=ncfile['time'].calendar)
        damage = imp.imp_mat[tc_i,:].toarray()
        damage_nc = np.zeros(grid_shape).ravel()
        damage_nc[nc_indls] = damage

        ncfile['time'][nc_i] = nc_date
        ncfile['id'][nc_i] = haz.event_name[tc_i]
        ncfile['damage_pc'][nc_i, :, :] = damage_nc.reshape(grid_shape)
        nc_i += 1
    ncfile.close()


if __name__ == "__main__":
    tc_haz = tc_wind_haz()
    write_windfield_nc(tc_haz)
    write_litpop_damage(tc_haz)
