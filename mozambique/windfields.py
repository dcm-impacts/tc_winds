
import datetime as dt
import logging
import os
import pathlib
import sys
import warnings

import geopandas as gpd
import numpy as np
import pandas as pd
import scipy.sparse as sp
import shapely.geometry
import xarray as xr

from climada.hazard import Centroids, TCTracks, TropCyclone
import climada.util.coordinates as u_coord

LOGGER = logging.getLogger('mozambique')
LOGGER.setLevel(logging.DEBUG)
LOGGER.propagate = False
if LOGGER.hasHandlers():
    for handler in LOGGER.handlers:
        LOGGER.removeHandler(handler)
FORMATTER = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
CONSOLE = logging.StreamHandler(stream=sys.stdout)
CONSOLE.setFormatter(FORMATTER)
LOGGER.addHandler(CONSOLE)

BASIN = "SI"  # Southern Indian basin
COUNTRY_ISO = "MOZ"  # Mozambique
YEAR_START, YEAR_END = 1981, 2019
RES_AS = 150

ERA5L_PATH = pathlib.Path("/home/tovogt/data/rainfall/ERA5-Land/")
ERA5L_FNAME = "total_precipitation_ERA5-Land_{year:04d}{month:02d}_daily.nc"

SHAPES_DIR = pathlib.Path("shapes")
VILLAGES_PATH = SHAPES_DIR / "villages_2017/BASE_DE_AE_CENSO_2017/BASE_COMPLETA_DE_AE_CENSO_2017.shp"
VILLAGES_KEY = "OBJECTID_1"
ADM2_PATH = SHAPES_DIR / "districts_2005/MOZ_adm2/MOZ_adm2.shp"
ADM2_KEY = "OBJECTID"
ADM3_PATH = SHAPES_DIR / "districts_2005/MOZ_adm3/MOZ_adm3.shp"
ADM3_KEY = "ID_3"


def vectorized_contains(shape, x, y):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        # suppress something about `np.bool`
        return shapely.vectorized.contains(shape, x, y)


def get_country_gridpoints():
    shape = gpd.read_file(VILLAGES_PATH).geometry.unary_union
    res_deg = RES_AS / 3600
    bounds = shape.envelope.buffer(res_deg).bounds
    lon_dim = np.arange(bounds[0] + 0.5 * res_deg, bounds[2], res_deg)
    lat_dim = np.arange(bounds[1] + 0.5 * res_deg, bounds[3], res_deg)
    lon, lat = [ar.ravel() for ar in np.meshgrid(lon_dim, lat_dim)]
    msk = vectorized_contains(shape, lon, lat)
    return lat[msk], lon[msk]


def points_in_shapes(shapefile, centroids, key):
    gdf = gpd.read_file(shapefile)
    assert gdf.crs == "epsg:4326"
    assert np.unique(gdf[key]).size == gdf.shape[0]
    LOGGER.info(f"Assign {centroids.size} centroids to {gdf.shape[0]} regions from {shapefile}...")
    lat, lon = centroids.lat, centroids.lon
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        # suppress something about geographic CRS
        gdf['area'] = gdf.geometry.area
    gdf = gdf.sort_values(by=['area'], ascending=False)
    region_id = np.full((lon.size,), -1, dtype=int)
    total_land = gdf.geometry.unary_union
    ocean_mask = ~vectorized_contains(total_land, lon, lat)
    region_id[ocean_mask] = -2
    for idx, region in gdf.iterrows():
        unset = (region_id == -1).nonzero()[0]
        select = vectorized_contains(
            region.geometry, lon[unset], lat[unset])
        region_id[unset[select]] = int(region[key])

    if (region_id < 0).sum() > 0:
        LOGGER.warning(f"Warning: There are unassigned ({(region_id == -1).sum()}) and "
              f"offshore ({(region_id == -2).sum()}) centroids.")
    reg_uniq = np.unique(region_id[region_id >= 0])
    reg_total = np.unique(gdf[key])
    if reg_uniq.size != reg_total.size:
        reg_missing = reg_total[~np.isin(reg_total, reg_uniq)]
        LOGGER.warning(f"Warning: No centroids are in the following {reg_missing.size} regions: {reg_missing}")
    return region_id


def prepare_ds(lat, lon, nevents):
    """Prepare a gridded xarray dataset for non-gridded lat/lon values

    Parameters
    ----------
    lat, lon : ndarrays of floats
        Centroid coordinates.
    nevents : int
        Number of events in the final data set.

    Returns
    -------
    ds : xr.Dataset
        Dimensions for events, longitude and latitude.
        More variables have to be added.
    indls : ndarray of ints
        The centroids of the original data are not necessarily on a grid, but
        the NetCDF format forces the output to be on a (not necessarily regular)
        grid.
        This output is for identification between the original centroids and
        the grid cells in the NetCDF data. If the coordinates of the original
        centroids are stored in two arrays (lat, lon) and the NetCDF dimensions
        are (ds_lat, ds_lon), then:

        >>> yf, xf = [c.flatten() for c in np.meshgrid(ds_lat, ds_lon, indexing='ij')]
        >>> assert np.all(lat == yf[indls])
        >>> assert np.all(lon == xf[indls])
    """
    lat_uniq, lat_inv = np.unique(lat, return_inverse=True)
    lon_uniq, lon_inv = np.unique(lon, return_inverse=True)
    grid_shape = (lat_uniq.shape[0], lon_uniq.shape[0])
    indls = np.ravel_multi_index((lat_inv, lon_inv), dims=grid_shape)

    ds = xr.Dataset(
        data_vars={
            'id': (["event"], ["0" * 13] * nevents),
            "time_end": (["event"], pd.date_range("2000-01-01", periods=nevents)),
        },
        coords={
            "lat": (["lat"], lat_uniq),
            "lon": (["lon"], lon_uniq),
            "time_begin": (["event"], pd.date_range("2000-01-01", periods=nevents)),
        },
    )

    ds.lat.attrs['units'] = 'degrees_north'
    ds.lat.attrs['standard_name'] = 'latitude'

    ds.lon.attrs['units'] = 'degrees_east'
    ds.lon.attrs['standard_name'] = 'longitude'

    ds.id.attrs['standard_name'] = 'storm ID in ibtracs database'

    return ds, indls


def centroids_box(centroids, buffer=0.0):
    cen_bounds = (centroids.lon.min(), centroids.lat.min(),
                  centroids.lon.max(), centroids.lat.max())
    return shapely.geometry.box(*cen_bounds).buffer(buffer).envelope


def tc_wind_haz():
    # generate centroids for country
    centroids = Centroids()
    centroids.set_lat_lon(*get_country_gridpoints())
    centroids.set_dist_coast(precomputed=True)
    moz_box = centroids_box(centroids, buffer=5.5)

    # read all tracks in basin and time period
    tr = TCTracks()
    tr.read_ibtracs_netcdf(year_range=(YEAR_START, YEAR_END), basin=BASIN, estimate_missing=True)

    # filter out those tracks that are too far off Mozambique
    tr_bounds = tr.get_bounds()
    mid_lon = 0.5 * (tr_bounds[0] + tr_bounds[2])
    tr_df = tr.to_geodataframe()
    tr_df.crs = "epsg:4326"
    tr_df = tr_df.to_crs({"proj": "longlat", "lon_wrap": mid_lon})
    tr_sel = tr_df.intersects(moz_box).values
    tr.data = [t for i, t in enumerate(tr.data) if tr_sel[i]]

    # filter out the track positions that are too far off Mozambique
    for i, track in enumerate(tr.data):
        tr_df = track.to_dataframe()
        tr_df = gpd.GeoDataFrame(
            tr_df, crs="epsg:4326",
            geometry=gpd.points_from_xy(tr_df.lon, tr_df.lat))
        tr_df = tr_df.drop(columns=['lon', 'lat'])
        tr_df = tr_df.to_crs({"proj": "longlat", "lon_wrap": mid_lon})
        tr_sel = tr_df.intersects(moz_box).values
        if tr_sel.sum() <= 1:
            tr_sel_imin = np.fmax(0, np.argmin(tr_sel) - 1)
            tr_sel[tr_sel_imin:tr_sel_imin + 2] = True
        tr.data[i] = track.sel(time=tr_sel)

    # interpolate to hourly resolution
    tr.equal_timestep(time_step_h=1)

    # compute wind fields
    tc = TropCyclone()
    tc.set_from_tracks(tr, centroids=centroids)

    # restrict to tcs with non-zero winds
    tr_sel = tc.intensity.sum(axis=1).A1
    tc = tc.select(event_names=[n for i, n in enumerate(tc.event_name) if tr_sel[i]])
    tr.data = [t for i, t in enumerate(tr.data) if tr_sel[i]]
    tc.check()

    return tr, tc


def tc_get_rain(tracks, winds):
    rains = []
    cen = winds.centroids
    for tc_i, tr in enumerate(tracks.data):
        period = tr.time.values[0], tr.time.values[-1]
        # nz_idx = winds.intensity[tc_i, :].nonzero()[1]
        nz_idx = slice(None)
        rain = np.zeros(winds.intensity.shape[1])
        rain[nz_idx] = era5l_total_rainf(period, cen.coord[nz_idx, :])
        rains.append(sp.csr_matrix(rain))
    rains = sp.vstack(rains)
    return rains


def era5l_total_rainf(period, points):
    pad = 0.1
    bounds_padded = (points[:, 1].min() - pad, points[:, 0].min() - pad,
                     points[:, 1].max() + pad, points[:, 0].max() + pad)
    d1, d2 = [d.astype('datetime64[D]') for d in period]
    days = np.arange(d1, d2 + np.timedelta64(1, 'D'))
    rain_raster = [era5l_daily_rainf(d, bounds_padded) for d in days]
    transform, rain_raster = rain_raster[0][1], np.sum([d for d, _ in rain_raster], axis=0)
    return u_coord.interp_raster_data(rain_raster, points[:, 0], points[:, 1], transform)


def era5l_daily_rainf(date, bounds):
    # in ERA5-Land, rainfall at 00:00:00 is accumulated value from previous day
    date += np.timedelta64(1, "D")

    day, month, year = dt64_to_dmy(date)
    path = ERA5L_PATH / ERA5L_FNAME.format(year=year, month=month)
    if bounds[0] < 0:
        bounds = (bounds[0] + 360, bounds[1], bounds[2] + 360, bounds[3])
    with xr.open_dataset(path) as rainf_ds:
        rainf_ds = rainf_ds.rename({"longitude": "lon", "latitude": "lat"})
        res = (rainf_ds.lon.values[1] - rainf_ds.lon.values[0],
               rainf_ds.lat.values[1] - rainf_ds.lat.values[0])
        rainf_ds = rainf_ds.sel(time=(rainf_ds.time.dt.day == day))
        mask_bounds = ((bounds[1] <= rainf_ds.lat)
                       & (rainf_ds.lat <= bounds[3])
                       & (bounds[0] <= rainf_ds.lon)
                       & (rainf_ds.lon <= bounds[2]))
        rainf_ds = rainf_ds.where(mask_bounds, drop=True)
        # convert from m to mm
        rainf = rainf_ds.tp.values[0, :, :] * 1000
        rainf[np.isnan(rainf)] = 0
        transform = [res[0], 0, rainf_ds.lon.values[0] - 0.5 * res[0],
                     0, res[1], rainf_ds.lat.values[0] - 0.5 * res[1]]
    return rainf, transform


def dt64_to_dmy(date):
    day = (date - date.astype('datetime64[M]') + 1).astype(int)
    month = date.astype('datetime64[M]').astype(int) % 12 + 1
    year = date.astype('datetime64[Y]').astype(int) + 1970
    return day, month, year


def write_windfield_nc(tracks, winds, rain, adm2):
    path = f"windfields_{YEAR_START}-{YEAR_END}_{COUNTRY_ISO}_{RES_AS}as.nc"

    LOGGER.info(f"Preparing {path}...")
    nevents = len(winds.date)
    ds, indls = prepare_ds(winds.centroids.lat, winds.centroids.lon, nevents)
    grid_shape = (ds.lat.size, ds.lon.size)
    var_shape = (nevents,) + grid_shape

    ds['wind'] = (["event", "lat", "lon"], np.zeros(var_shape, dtype=np.float64))
    ds.wind.attrs['standard_name'] = 'TC wind speed'
    ds.wind.attrs['units'] = 'm/s'

    ds['rain'] = xr.zeros_like(ds.wind)
    ds.rain.attrs['standard_name'] = "total rain fall per grid cell"
    ds.rain.attrs['units'] = "mm"

    adm2_nc = np.zeros(grid_shape).ravel()
    adm2_nc[indls] = adm2
    ds['adm2'] = (["lat", "lon"], adm2_nc.reshape(grid_shape))
    ds.adm2.attrs['standard_name'] = "districts (admin-2, 2005)"
    ds.adm2.attrs['units'] = ADM2_KEY

    ds['year_last_affected'] = (["lat", "lon"], np.zeros(grid_shape, dtype=np.uint16))
    ds.year_last_affected.attrs['standard_name'] = "Most recent year a TC hit each cell"

    LOGGER.info(f"Writing {len(winds.date)} windfields...")
    for tc_i, tr in enumerate(tracks.data):
        ds.time_begin.values[tc_i] = tr.time.values[0]
        ds.time_end.values[tc_i] = tr.time.values[-1]
        ds.id.values[tc_i] = tr.sid

        wind_nc = np.zeros(grid_shape).ravel()
        wind_nc[indls] = winds.intensity[tc_i, :].toarray()
        wind_nc = wind_nc.reshape(grid_shape)
        ds.wind.values[tc_i, :, :] = wind_nc

        rain_nc = np.zeros(grid_shape).ravel()
        rain_nc[indls] = rain[tc_i, :].toarray()
        rain_nc = rain_nc.reshape(grid_shape)
        ds.rain.values[tc_i, :, :] = rain_nc

        ds.year_last_affected.values[:] = np.fmax(
            ds.year_last_affected.values,
            dt64_to_dmy(tr.time.values[0])[2] * (wind_nc > 0).astype(np.uint16))
    ds.to_netcdf(path, encoding={
        "wind": {"zlib": True}, "rain": {"zlib": True}})


if __name__ == "__main__":
    tc_tracks, tc_winds = tc_wind_haz()
    tc_rain = tc_get_rain(tc_tracks, tc_winds)
    adm2 = points_in_shapes(ADM2_PATH, tc_winds.centroids, ADM2_KEY)
    write_windfield_nc(tc_tracks, tc_winds, tc_rain, adm2)
