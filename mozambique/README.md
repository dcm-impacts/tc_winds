
January 19, 2021
================

Hazem and Kati Kraehnert ask for data about TCs hitting Mozambiqe in 2003-2005,
including wind/surge/rain.

Special attention: TC Japhet (2003056S21042)

Which regions are most affected? Inland? Coastal? Wind? Surge? Rain

Result
------

The two tropical cyclone events that hit Mozambique in 2002-2005 are the following:

**Severe Tropical Storm Delfina**
* 2002-12-30 until 2003-01-14 with most impact in the first 3 days
* Direct effects on Mozambique for 14 days
* https://en.wikipedia.org/wiki/Tropical_Storm_Delfina
* http://ibtracs.unca.edu/index.php?name=v04r00-2002364S16045

**Intense Tropical Cyclone Japhet**
* 2003-02-27 until 2003-03-05
* Direct effects on Mozambique for 7 days
* https://en.wikipedia.org/wiki/Cyclone_Japhet
* http://ibtracs.unca.edu/index.php?name=v04r00-2003056S21042

For each village and adm2/adm3 district and for each of the two TC events, there is the maximum
wind (in m/s) and the maximum rain (in mm) in the CSV files. Winds are truncated at 17.5 m/s (34 knots)
and rain is truncated at 100 mm.

April 29, 2021
==============

Hazem and Lukas explain that we actually need data for all events hitting Mozambique
between 1980 and 2010, with beginning and end date for each event (for annual aggregation).

We would like to have it as lat-lon (grid) data and aggregated according to the shape files.

The resolution of the grid data should be 150 as (as with Inga's data).

Problem: The ERA5-Land only starts in 1981 (instead of 1980) -> We provide 1981-2010.
